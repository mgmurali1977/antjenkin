/**
 * @File Name          : myOuterClass.cls
 * @Description        : 
 * @Author             : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 1/28/2020, 1:00:01 AM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    1/25/2020   ChangeMeIn@UserSettingsUnder.SFDoc     Initial Version
**/
public class myOuterClass {
   // Additional myOuterClass code here
   class myInnerClass {
     // myInnerClass code here changed by vscode git testing again and again and again and again
   }
}